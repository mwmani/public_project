*** Settings ***
Documentation  This file contains locators for elements on contextures site

*** Variables ***
${Table_Locator}            xpath:/html/body/div[1]/div/div[1]/table[14]/tbody
${Table_Row_Locator}       xpath:/html/body/div[1]/div/div[1]/table[14]/tbody/tr
${Table_Cols_Locator}          xpath:/html/body/div[1]/div/div[1]/table[14]/tbody/tr[1]/th
${Download_link}            xpath:/html/body/div/div/div[1]/table[12]/tbody/tr/td/ul/li[1]/a
