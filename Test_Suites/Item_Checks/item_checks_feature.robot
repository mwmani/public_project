*** Settings ***
Library     SeleniumLibrary
Library     Collections
Library     item_checks.py
Resource    Config/login_page/login_page_locators.robot
Resource    Utilities/login_page_functionalties/login_page_functionality.robot
Resource    Config/Item_Checks/item_checks_locators.robot
Resource    Utilities/Item_Checks/item_checks_functionality.robot
Suite Setup    Run Keyword    Launch Browser
Suite Teardown     Close Browser

*** Variables ***
${site_address}         https://contextures.com/xlsampledata01.html#data

*** Test Cases ***
TC1 Check how many different items are there
    [Documentation]  This test case will verify how many different items are there in the list
    [Tags]      Sanity
    Given Scroll To Items List
    When Get number of rows
    When Get number of cols
    Then Remove Duplicates From The List And Return Count


TC2 Check If There Is An item With Less Than 5 Units
    [Documentation]  This test case will verify if there is an item with less than 5 units
    [Tags]      Smoke     Regression
    Given Scroll To Items List
    Then Check The Item Count Is Less Than 5


TC3 Check If There Is A Pencil With Less Than 5 Units
    [Documentation]  This test case will verify if there is a pencil with less than 5 units
    [Tags]      Regression
    Given Scroll To Items List
    Then Verify Elements On Login Page
    Then Verify If There Is A Pencil With Less Than 5 Units


TC4 Check What Is The Most Expensive Item On The List
    [Documentation]  This test case will verify the most expensive item on the list
    [Tags]      Smoke
    Given Scroll To Items List
    Then Verify The Most Expensive Item On The List


TC5 Check That Excel Download Works
    [Documentation]  This test case will verify that user can download excel file
    [Tags]      Sanity
    Given Scroll To Items List
    Then Excel Download Feature Check
