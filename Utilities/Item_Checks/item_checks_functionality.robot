*** Settings ***
Documentation  This file contains keywords related to item checks functionality
Resource  Config/Item_Checks/item_checks_locators.robot
Library   Collections

*** Keywords ***
Scroll To Items List
    [Documentation]  This keyword will scroll till the element is visible
    Scroll Element Into View   ${Table_Locator}


Get number of rows
    [Documentation]  This keyword will get the number of rows
    ${number_of_rows}   Get Element Count  ${Table_Row_Locator}
    [Return]    ${number_of_rows}


Get number of cols
    [Documentation]  This keyword will get the number of cols
    ${number_of_cols}   Get Element Count  ${Table_Cols_Locator}
    [Return]    ${number_of_cols}


Remove Duplicates From The List And Return Count
    [Documentation]  This keyword will remove duplicates from the list and return count
    ${List_Of_Items}    Create List
    :FOR  ${i} In Range  1  ${number_of_rows}
    \       ${items}    Get Text   /html/body/div[1]/div/div[1]/table[14]/tbody/tr["${i}"]/th[4]
    \       Append To List   ${List_Of_Items}   ${items}
    Remove Duplicates   ${List_Of_Items}
    ${Number_Of_Unique_Items}   Count Values In List    ${List_Of_Items}
    [Return]    ${Number_Of_Unique_Items}


Check The Item Count Is Less Than 5
    [Documentation]  This keyword will check if item count is less than 5
    [Arguments]    @{columns}
    Get number of cols
    Get number of rows
    ${length}    Get Length    ${verify_values}
    Set Test Variable    ${count}    0
    ${index}    Set Variable     0
    : FOR    ${col}    IN    @{columns}
    \    ${value}=    Get Text    ${col}
    \    Run Keyword If    '${value}'== '${verify_values[${index}]}'    Increment Count    ELSE    Exit For Loop
    \    ${index}=    Evaluate    ${index}+1
    \    Run Keyword If    ${length} <= 5   Set Status SUCCESS and Exit


Excel Download Feature Check
    [Documentation]  This keyword will check if excel file is downloaded or not
    ${chrome options}=  Evaluate  sys.modules['selenium.webdriver'].ChromeOptions()  sys, selenium.webdriver
    ${prefs}  Create Dictionary
    ...  download.default_directory=/Users/${username}/PycharmProjects/Robot-Framework/Resources/Download
    Call Method  ${chrome options}  add_experimental_option  prefs  ${prefs}
    Create Webdriver  Chrome  chrome_options=${chrome options}
    Goto  ${site_address}
    Click Element       ${Download_link}
    Sleep  5s
    ${files}  List Files In Directory  /Users/${user_name}/PycharmProjects/Robot-Framework/Resources/Download
    Length Should Be  ${files}  1


Verify The Most Expensive Item On The List
    [Documentation]  This keyword will find most expensive item from the list and return the value
    ${List_Of_Items}    Create List
    :FOR  ${i}  In Range  1  ${number_of_rows}
    \       ${items}    Get Text   /html/body/div[1]/div/div[1]/table[14]/tbody/tr["${i}"]/th[6]
    \       Append To List   ${List_Of_Items}   ${items}
    \       Sort List    ${List_Of_Items}
    \       ${Max_Value_Item}   Get From List   ${List_Of_Items}    -1
    \       ${e}     Get Index From List     ${Max_Value_Item}
    ${Max_Value_Item_Locator}     Set Variable     /html/body/div[1]/div/div[1]/table[14]/tbody/tr["${e}"]/th[4]
    ${Item_With_Max_Value}      Get Variable Value      ${Max_Value_Item_Locator}


Verify If There Is A Pencil With Less Than 5 Units
    [Documentation]  This keyword will check if there is a pencil with less than 5 units
    [Arguments]    @{columns}
    Get number of cols
    Get number of rows
    ${List_Of_Items}    Create List
    :FOR  ${i} In Range  1  ${number_of_rows}
    \       ${items}    Get Text   /html/body/div[1]/div/div[1]/table[14]/tbody/tr["${i}"]/th[4]
    \       ${values}    Get Text   /html/body/div[1]/div/div[1]/table[14]/tbody/tr["${i}"]/th[5]
    \       IF  '${items}' == 'Pencil' and '${values}' < 5  log to console  \nThere is a pencil with less than 5 units
    \       ELSE    log to console    \nThere is a pencil with less than 5 units
