*** Settings ***
Documentation  This file contains keywords related to login page
Library        SeleniumLibrary
Resource       Config/login_page/login_page_locators.robot

*** Variables ***
${headless_chrome_browser}      headlesschrome
${chrome}       chrome
${site_address}         https://contextures.com/xlsampledata01.html#data

*** Keywords ***
Launch Browser
    [Documentation]   This keyword is used to launch browser with site address
    Open Browser  ${site_address}   ${chrome}
    Maximize Browser Window

Verify Elements On Login Page
    [Documentation]  This keyword will verify elements are present on login page
    Page Should Contain Element     ${Contextures_Locator}
    Page Should Contain Element     ${Home_Text_Locator}
    Capture Page Screenshot